module.exports = (function () {
	'use strict'
	
	// config vars
	const wholeDir    = './'
	const app         = './app'
	const nodeModules = '../node_modules'
	const scripts     = `${app}/scripts`
	const sass        = `${app}/styles`
	const port        = 3002
	
	let data = {
		browserSyncPort:         port,
		cordovaPath:             'cordova',
		defaultPlatform:         'ios',
		excludedBowerComponents: ['es5-shim', 'json3'],
		base:                    app,
		wholeDir:                wholeDir,
		mainFile:                `${app}/index.html`,
		mainSassFile:            `${sass}/main.scss`,
		routesFiles:             `${app}/scripts/_routes.js`,
		e2eBaseUrl:              `http://localhost:${port}/`,
		styles:                  `${app}/styles/`,
		stylesF:                 [
			`${app}/styles/**/_*.{scss,sass,less}`,
			`${scripts}/**/*.{scss,sass,less}`,
			`${nodeModules}/bootstrap/dist/dist/css/bootstrap.min.css`
		],
		stylesAllF:              [
			`${app}/styles/**/*.{scss,sass,less}`,
			`${scripts}/**/*.{scss,sass,less}`,
			`${nodeModules}/slick-carousel/slick/slick.scss`
		],
		scripts:                 `${app}/scripts/`,
		scriptsF:                [
			// modules first
			`${app}/scripts/**/_*.js`,
			`${app}/scripts/**/*.js`,
			`!${app}/scripts/**/*.spec.js`,
			`${nodeModules}/jquery/dist/jquery.min.js`,
			`${nodeModules}/bootstrap/dist/js/bootstrap.min.js`,
			`${nodeModules}/angularfire/dist/angularfire.min.js`,
			`${nodeModules}/slick-carousel/slick/slick.min.js`
		],
		scriptsAllF:             `${app}/scripts/**/*.js`,
		scriptTestsF:            `${app}/scripts/**/*.spec.js`,
		html:                    `${app}/scripts/`,
		htmlF:                   [`${app}/scripts/**/*.html`],
		images:                  `${app}/images/`,
		imagesF:                 `${app}/images/**/*.*`,
		fonts:                   `${app}/fonts/`,
		fontsF:                  `${app}/fonts/**/*.*`,
		tmp:                     './.tmp',
		dist:                    'dist',
		wwwDestination:          '',
		karmaConf:               './karma.conf.js',
		karmaConfE2E:            './karma-e2e.conf.js'
	}
	
	data.allHtmlF = data.htmlF.slice()
	data.allHtmlF.push(data.mainFile)
	
	return data
})()
