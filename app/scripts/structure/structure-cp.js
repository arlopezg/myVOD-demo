/**
 * @ngdoc component
 * @name accedowebtest.component:structure
 * @description
 * # structure
 */

(() => {
  'use strict'

  class StructureCtrl {
    /* @ngInject */
    constructor () {
    }
  }

  angular.module('accedowebtest').component('structure', {
    templateUrl:      'scripts/structure/structure-cp.html',
    controller:       StructureCtrl,
    controllerAs:     '$ctrl',
    bindToController: {}
  })

  // hacky fix for ff
  StructureCtrl.$$ngIsClass = true
})()
