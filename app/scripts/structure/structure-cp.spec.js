'use strict'

describe('Component: structure', () => {

  // load the directive's module
  beforeEach(module('accedowebtest'))
  beforeEach(module('templates'))

  let element
  let scope

  beforeEach(inject(($rootScope) => {
    scope = $rootScope.$new()
  }))

  it('should do something', inject(($compile) => {
    element = $compile('<structure></structure>')(scope)
    scope.$digest()
    expect(true).toBe(true)
  }))
})