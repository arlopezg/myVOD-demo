'use strict'

describe('Controller: DashboardCtrl', () => {

  // load the controller's module
  beforeEach(module('accedowebtest'))

  let DashboardCtrl
  let scope

  // Initialize the controller and a mock scope
  beforeEach(inject(($controller, $rootScope) => {
    scope         = $rootScope.$new()
    DashboardCtrl = $controller('DashboardCtrl', {
      $scope: scope
      // place mocked dependencies here
    })
  }))

  it('should ...', () => {
    expect(true).toBe(true)
  })
})