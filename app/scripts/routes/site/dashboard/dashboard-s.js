/**
 * @ngdoc service
 * @name accedowebtest.Dashboard
 * @description
 * # Dashboard
 * Service in the accedowebtest.
 */

(() => {
  'use strict'

  class Dashboard {
    /* @ngInject */
    constructor () {
    }
  }

  angular.module('accedowebtest').service('Dashboard', Dashboard)

  // hacky fix for ff
  Dashboard.$$ngIsClass = true
})()
