/**
 * @ngdoc function
 * @name accedowebtest.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the accedowebtest
 */

(function () {
  'use strict'
  angular.module('accedowebtest').controller('DashboardCtrl', controller)

  controller.$inject = ['MoviesService', '$stateParams']

  function controller (MoviesService, $stateParams) {
    /*********** Variables *******************/
    let vm = this
    if ($stateParams.tag)
      vm.search = $stateParams.tag
    /*****************************************/

    /*********** Functions menu ***********/
    vm.movieIndex = movieIndex
    vm.goToMovie = goToMovie
    /*****************************************/

    /*********** Processes ********************/
    getMovies()
    getHistory()

    /*****************************************/

    /*********** Specific functions ********/
    function movieIndex (id) {
      return vm.movies.findIndex(movie => movie.id === id)
    }

    function goToMovie (id) {
      MoviesService.goToMovieById(id)
    }

    /*****************************************/

    /*********** General functions **********/
    function getMovies () {
      vm.movies = MoviesService.getAll()
    }

    function getHistory () {
      vm.history = []
      MoviesService.getHistory().map((movie) => {
        vm.history.push(MoviesService.getMovieByIndex(movie.index))
      })
      vm.history.reverse()
    }

    /*****************************************/
  }
})()
