/**
 * @ngdoc function
 * @name accedowebtest.controller:MovieDetailsCtrl
 * @description
 * # MovieDetailsCtrl
 * Controller of the accedowebtest
 */

(function () {
	'use strict'
	angular.module('accedowebtest').controller('ViewCtrl', controller)
	
	controller.$inject = ['MoviesService', '$stateParams', '$state']
	
	function controller (MoviesService, $stateParams, $state) {
		/*********** Variables *******************/
		let vm = this
		/*****************************************/
		
		/*********** Functions menu ***********/
		vm.backToHome = backToHome
		/*****************************************/
		
		/*********** Processes ********************/
		getMovieData($stateParams.index)
		getAllMovies()
    markAsWatched($stateParams.index)
		/*****************************************/
		
		/*********** Specific functions ********/
		function backToHome () {
			$state.go('site.dashboard')
		}
		/*****************************************/
		
		/*********** General functions **********/
		function getAllMovies () {
			vm.movies = MoviesService.getAll()
		}
		function getMovieData (index) {
			vm.movie = MoviesService.getMovieByIndex(index)
		}

    function markAsWatched (index) {
      MoviesService.markAsWatched(index, new Date().getTime())
    }
		
		/*****************************************/
	}
})()