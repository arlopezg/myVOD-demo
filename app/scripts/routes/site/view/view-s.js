/**
 * @ngdoc service
 * @name accedowebtest.View
 * @description
 * # View
 * Service in the accedowebtest.
 */

(() => {
    'use strict';

    class View {
        /* @ngInject */
        constructor(){
        }
    }

    angular
        .module('accedowebtest')
        .service('View', View);

    // hacky fix for ff
    View.$$ngIsClass = true;
})();
