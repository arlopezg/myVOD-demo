'use strict';

describe('Controller: ViewCtrl', () => {

    // load the controller's module
    beforeEach(module('accedowebtest'));

    let ViewCtrl;
    let scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(($controller, $rootScope) => {
        scope = $rootScope.$new();
        ViewCtrl = $controller('ViewCtrl', {
             $scope: scope
             // place mocked dependencies here
        });
    }));

    it('should ...', () => {
         expect(true).toBe(true);
    });
});