/**
 * @ngdoc function
 * @name accedowebtest.controller:MyHistoryCtrl
 * @description
 * # MyHistoryCtrl
 * Controller of the accedowebtest
 */

(function () {
  'use strict'
  angular.module('accedowebtest').controller('MyHistoryCtrl', controller)

  controller.$inject = ['MoviesService']

  function controller (MoviesService) {
    /*********** Variables *******************/
    let vm    = this
    vm.movies = []
    /*****************************************/

    /*********** Functions menu ***********/
    vm.resetHistory = resetHistory
    vm.goToMovie = goToMovie
    /*****************************************/

    /*********** Processes ********************/
    getHistory()

    /*****************************************/

    /*********** Specific functions ********/
    function resetHistory (index) {
      MoviesService.resetHistory(index)
      getHistory()
    }

    function goToMovie (id) {
      MoviesService.goToMovieById(id)
    }

    /*****************************************/

    /*********** General functions **********/
    function getHistory () {
      vm.history = []
      MoviesService.getHistory().map((movie) => {
        vm.history.push(MoviesService.getMovieByIndex(movie.index))
      })
    }

    /*****************************************/
  }
})()
