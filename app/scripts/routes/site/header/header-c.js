/**
 * @ngdoc function
 * @name accedowebtest.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the accedowebtest
 */

(() => {
  'use strict'

  class HeaderCtrl {
    /* @ngInject */
    constructor () {
    }
  }

  angular.module('accedowebtest').controller('HeaderCtrl', HeaderCtrl)

  // hacky fix for ff
  HeaderCtrl.$$ngIsClass = true

})()
