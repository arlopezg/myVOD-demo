'use strict'

describe('Controller: HeaderCtrl', () => {

  // load the controller's module
  beforeEach(module('accedowebtest'))

  let HeaderCtrl
  let scope

  // Initialize the controller and a mock scope
  beforeEach(inject(($controller, $rootScope) => {
    scope      = $rootScope.$new()
    HeaderCtrl = $controller('HeaderCtrl', {
      $scope: scope
      // place mocked dependencies here
    })
  }))

  it('should ...', () => {
    expect(true).toBe(true)
  })
})