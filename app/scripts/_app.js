/**
 * @ngdoc overview
 * @name accedowebtest
 * @description
 * # accedowebtest
 *
 * Main module of the application.
 */

(() => {
	'use strict'
	
	angular.module('accedowebtest', [
		'ngAnimate',
		'ngAria',
		'ngResource',
		'ui.router',
		'ngFabForm',
		'firebase',
		'ngStorage',
		'ui.carousel'
	])
})()