/**
 * @ngdoc factory
 * @name accedowebtest.Unique
 * @description
 * # Unique
 * Factory in the accedowebtest.
 */

(() => {
    'use strict';

    class Unique {
        /* @ngInject */
        constructor(){
        }
    }

    angular
        .module('accedowebtest')
        .factory('Unique', Unique);

    // hacky fix for ff
    Unique.$$ngIsClass = true;
})();
