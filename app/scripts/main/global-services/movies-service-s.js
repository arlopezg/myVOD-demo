/**
 * @ngdoc service
 * @name accedowebtest.MoviesService
 * @description
 * # MoviesService
 * Service in the accedowebtest.
 */

(function () {
  'use strict'
  angular.module('accedowebtest').service('MoviesService', service)

  service.$inject = [
    'FirebaseService', '$firebaseObject', '$firebaseArray', '$localStorage',
    '$state'
  ]

  function service (FirebaseService, $firebaseObject, $firebaseArray,
                    $localStorage, $state) {
    /*********** Variables *******************/
    const movies_ref     = FirebaseService.getMoviesReference()
    const categories_ref = FirebaseService.getCategoriesReference()

    /*********** Fin de Variables ************/

    /*********** Menu de funciones ***********/
    /*********** Fin menu de funciones********/

    /*********** Procesos ********************/

    return {
      getAll:          getAll,
      getMovieByIndex: getMovieByIndex,
      markAsWatched:   markAsWatched,
      getHistory:      getHistory,
      goToMovieById:   goToMovieById,
      resetHistory:    resetHistory
    }

    /*********** Fin de process **************/

    /***********Funciones especificas ********/
    function getAll (callback) {
      if (!callback)
        return $firebaseArray(movies_ref)
      else
        callback($firebaseArray(movies_ref))
    }

    function getMovieByIndex (id) {
      return $firebaseObject(movies_ref.child(id))
    }

    function goToMovieById (id) {
      getAll().$loaded().then((movies) => {
        console.log(movies)
        $state.go('site.view',
          {'index': movies.findIndex(movie => movie.id === id)})
      })
    }

    function markAsWatched (index, id, time) {
      if (!$localStorage.watchedVideos)
        $localStorage.watchedVideos = []
      if (id)
        $localStorage.watchedVideos.push({index, id, time})
    }

    function getHistory () {
      if (!$localStorage.watchedVideos)
        $localStorage.watchedVideos = []
      return $localStorage.watchedVideos
    }

    function resetHistory (index) {
      if ($localStorage.watchedVideos)
        if (!index)
          $localStorage.watchedVideos = []
        else
          $localStorage.watchedVideos.splice(index, 1)
    }

    /*********** Fin Funciones especificas ***/

    /*********** Funciones Generales **********/
    /***********Fin de funciones generales*****/
  }
})()
