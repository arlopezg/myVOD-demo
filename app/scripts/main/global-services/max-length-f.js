/**
 * @ngdoc factory
 * @name accedowebtest.MaxLength
 * @description
 * # MaxLength
 * @example <ANY>{{ var | maxLength: (<integer>) }}</ANY>
 * Factory in the accedowebtest.
 */

angular.module('accedowebtest').filter('maxLength', filter)

filter.$inject = []

function filter () {
  return function (text, limit) {
    let changedString = String(text).replace(/<[^>]+>/gm, '')
    let length        = changedString.length
    let suffix        = '...'

    return length > limit
      ? changedString.substr(0, limit - 1) + suffix
      : changedString
  }
}