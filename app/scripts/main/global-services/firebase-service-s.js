/**
 * @ngdoc service
 * @name accedowebtest.FirebaseService
 * @description
 * # FirebaseService
 * Service in the accedowebtest.
 */

(function () {
	'use strict'
	angular.module('accedowebtest').service('FirebaseService', service)
	
	service.$inject = ['firebase']
	
	function service (firebase) {
		/*********** Variables *******************/
		/*********** Fin de Variables ************/
		
		/*********** Menu de funciones ***********/
		/*********** Fin menu de funciones********/
		
		/*********** Procesos ********************/
		return {
			getMoviesReference:     getMoviesReference,
			getCategoriesReference: getCategoriesReference
		}
		
		/*********** Fin de process **************/
		
		/***********Funciones especificas ********/
		function getMoviesReference () {
			return firebase.database().ref('entries')
		}
		
		function getCategoriesReference () {
			return firebase.database().ref('categories')
		}
		
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		/*********** Fin de funciones generales ***/
	}
})()