/**
 * @ngdoc overview
 * @name accedowebtest.routes
 * @description
 * # accedowebtest.routes
 *
 * Routes module. All app states are defined here.
 */

(() => {
  'use strict'

  angular.module('accedowebtest').config(router)

  /* @ngInject */
  function router ($stateProvider, $urlRouterProvider, $locationProvider) {

    $locationProvider.hashPrefix('')

    $stateProvider.state('site', {
      name:  'site',
      url:   '/',
      views: {
        '':             {
          templateUrl: 'scripts/structure/structure-cp.html'
        },
        'header@site':  {
          templateUrl: 'scripts/routes/site/header/header-c.html'
        },
        'content@site': {
          templateUrl: 'scripts/routes/site/dashboard/dashboard-c.html',
          controller:  'DashboardCtrl as vm'
        },
        'footer@site':  {
          templateUrl: 'scripts/routes/site/footer/footer-c.html'
        }
      }
    }).state('site.dashboard', {
      url:   'dashboard',
      name:  'dashboard',
      views: {
        'content@site': {
          templateUrl: 'scripts/routes/site/dashboard/dashboard-c.html',
          controller:  'DashboardCtrl as vm'
        }
      }
    }).state('site.movieDetails', {
        url:  'movie-details/:id',
        name: 'site.movie-details'
      }
    ).state('site.view', {
      url:   'view/:index',
      views: {
        'content@site': {
          templateUrl: 'scripts/routes/site/view/view-c.html',
          controller:  'ViewCtrl as vm'
        }
      }
    }).state('site.myHistory', {
      name:  'myHistory',
      url:   'my-history/',
      views: {
        'content@site': {
          templateUrl: 'scripts/routes/site/my-history/my-history-c.html',
          controller:  'MyHistoryCtrl as vm'
        }
      }
    }).state('site.search', {
      name:  'search',
      url:   'search',
      views: {
        'content@site': {
          templateUrl: 'scripts/routes/site/search/search-c.html',
          controller:  'DashboardCtrl as vm'
        }
      }
    })
    /* STATES-NEEDLE - DO NOT REMOVE THIS */
  }
})
()
